drop table IF EXISTS `question`;
create table `question`
(
    id      int auto_increment not null comment 'ID'
        primary key,
    title   varchar(50) not null comment 'title',
    options text       not null comment 'option',
    answer  int         not null comment 'answer'
);

drop table IF EXISTS `user_answer`;
create table `user_answer`
(
    question_id     int not null comment 'ID,question id'
        primary key,
    user_id         int not null comment 'User id',
    selected_answer int not null comment 'User answer'
);