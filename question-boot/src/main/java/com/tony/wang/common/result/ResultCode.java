package com.tony.wang.common.result;

import lombok.Getter;

@Getter
public enum ResultCode {

    SUCCESS(200, "Success"),

    FAILED(500, "Failed");

    private final Integer code;

    private final String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
