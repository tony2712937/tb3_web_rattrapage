package com.tony.wang.common.result;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Result implements Serializable {

    private Integer code;

    private String message;

    private Object data;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Result success(Object data) {
        return new Result(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    public static Result success() {
        return new Result(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), null);
    }

    public static Result failed(String msg) {
        return new Result(ResultCode.FAILED.getCode(), msg, null);
    }

    public static Result failed(Integer code, String msg) {
        return new Result(code, msg, null);
    }

    public static Result failed() {
        return failed(ResultCode.FAILED.getMessage());
    }

    public static Result failed(Integer code, String msg, Object data) {
        return new Result(code, msg, data);
    }

}
