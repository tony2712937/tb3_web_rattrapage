package com.tony.wang.vo;


import lombok.Data;

@Data
public class QuestionVO {

    private Integer id;

    private String title;

    private String option0;

    private String option1;

    private String option2;

    private String option3;

    private Integer answer;

}
