package com.tony.wang.entity;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

import java.util.Arrays;

@Data
@TableName(value = "question", autoResultMap = true)
public class Question {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String title;

    @TableField(value = "options", typeHandler = JacksonTypeHandler.class)
    private String[] options;

    private Integer answer;


    public void check() {
        if (StrUtil.isBlank(title)) {
            throw new RuntimeException("title of question can't be empty");
        }
        if (CollUtil.isEmpty(Arrays.asList(options))) {
            throw new RuntimeException("Option of question can't be empty");
        }
        if (ObjectUtil.isNull(answer)) {
            throw new RuntimeException("answer of question can't be empty");
        }
    }
}
