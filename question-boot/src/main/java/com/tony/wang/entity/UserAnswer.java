package com.tony.wang.entity;


import lombok.Data;


@Data
public class UserAnswer {

    private Integer id;

    private Integer questionAnswer;
}
