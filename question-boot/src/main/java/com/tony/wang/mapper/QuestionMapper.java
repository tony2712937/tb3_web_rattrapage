package com.tony.wang.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tony.wang.entity.Question;

import java.util.List;


public interface QuestionMapper extends BaseMapper<Question> {


    List<Question> listQuestion();
}

