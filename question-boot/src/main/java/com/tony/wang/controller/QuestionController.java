package com.tony.wang.controller;


import com.tony.wang.common.result.Result;
import com.tony.wang.entity.Question;
import com.tony.wang.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/vl/questions")
public class QuestionController {
    private final QuestionService questionService;

    @GetMapping("/getById/{id}")
    public Result getById(@PathVariable(value = "id") Integer id) {
        return Result.success(questionService.getQuestionById(id));
    }

    @GetMapping("/list")
    public Result list() {
        return Result.success(questionService.listQuestion());
    }

    @PostMapping("/add")
    public Result add(@RequestBody Question question) {
        questionService.add(question);
        return Result.success();
    }

    @PutMapping("/edit")
    public Result edit(@RequestBody Question question) {
        questionService.edit(question);
        return Result.success();
    }

    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable(value = "id") Integer id) {
        questionService.del(id);
        return Result.success();
    }

}

