package com.tony.wang.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tony.wang.entity.Question;
import com.tony.wang.mapper.QuestionMapper;
import com.tony.wang.vo.QuestionVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class QuestionService extends ServiceImpl<QuestionMapper, Question> {


    public List<Question> listQuestion() {
        return baseMapper.listQuestion();
//        return list();
    }

    public void add(Question question) {
        question.check();

        save(question);
    }

    public void edit(Question question) {
        if (ObjectUtil.isNull(question.getId())) {
            throw new RuntimeException("id can't be empty");
        }
        question.check();

        updateById(question);
    }

    public void del(Integer id) {
        if (ObjectUtil.isNull(id)) {
            throw new RuntimeException("id can't be empty");
        }

        removeById(id);
    }


    public QuestionVO getQuestionById(Integer id) {
        if (ObjectUtil.isNull(id)) {
            return new QuestionVO();
        }
        Question dbQuestion = getById(id);
        if (ObjectUtil.isNull(dbQuestion)) {
            throw new RuntimeException("Data not exist");
        }

        QuestionVO questionVO = new QuestionVO();
        questionVO.setId(id);
        questionVO.setTitle(dbQuestion.getTitle());
        String[] options = dbQuestion.getOptions();
        questionVO.setOption0(options[0]);
        questionVO.setOption1(options[1]);
        questionVO.setOption2(options[2]);
        questionVO.setOption3(options[3]);
        questionVO.setAnswer(dbQuestion.getAnswer());
        return questionVO;
    }
}
