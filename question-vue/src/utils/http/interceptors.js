import { isNullOrUndef } from "@/utils/is"
export function reqResolve(config) {
  if (config.method === "get") {
    config.params = { ...config.params, t: new Date().getTime() }
  }

  return config
}

export function reqReject(error) {
  return Promise.reject(error)
}

export function resResolve(response) {
  return response
}

export function resReject(error) {
  let { code, message } = error.response?.data || {}
  if (isNullOrUndef(code)) {
    code = -1
    message = "error！"
  }
  console.error(`【${code}】 ${error}`)
  return Promise.reject({ code, message, error })
}
