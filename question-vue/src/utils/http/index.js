import axios from "axios"
import { resResolve, resReject, reqResolve, reqReject } from "./interceptors"

export function createAxios(options = {}) {
  const defaultOptions = {
    baseURL: "/api",
    timeout: 12000
  }
  const service = axios.create({
    ...defaultOptions,
    ...options
  })
  service.interceptors.request.use(reqResolve, reqReject)
  service.interceptors.response.use(resResolve, resReject)
  return service
}

// 默认axios
export const defAxios = createAxios()
