import { createRouter, createWebHistory } from "vue-router"
import { basicRoutes as routes } from "./routes"

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior: () => ({ left: 0, top: 0 })
})

