export const basicRoutes = [
  {
    name: "homepage",
    path: "/",
    component: () => import("@/views/index.vue"),
    children:[
      {
        name: "list",
        path: "",
        component: () => import("@/views/ListQuestion.vue"),
      },
      {
        name: "add",
        path: "/add",
        component: () => import("@/views/AddOrUpdateQuestion.vue"),
      },
      {
        name: "update",
        path: "/update/:id",
        component: () => import("@/views/AddOrUpdateQuestion.vue"),
      }
    ]
  },
]
