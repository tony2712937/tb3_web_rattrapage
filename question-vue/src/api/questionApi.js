import { defAxios as request } from "@/utils/http/index.js"

export default {
  list() {
    return request({
      url: "/api/vl/questions/list",
      method: "get",
    })
  },
  getById(id) {
    return request({
      url: "/api/vl/questions/getById/" + id,
      method: "get",
    })
  },
  add(data) {
    return request({
      url: "/api/vl/questions/add",
      method: "post",
      data
    })
  },
  edit(data) {
    return request({
      url: "/api/vl/questions/edit",
      method: "put",
      data
    })
  },
  remove(id) {
    return request({
      url: "/api/vl/questions/remove/" + id,
      method: "delete"
    })
  }
}
